<br>
<img style="float: right;" src="https://gitlab.com/pedrosimpas/pedrosimpas/-/raw/main/MyProfilePic.jpeg" width="120">
<br>
<br>
<h1> Pedro Pasquini </h1> 
<h5> <span style="color:gray"> Curriculum Vitae </span> 
 </h5>
 <h6> <span style="color:gray"> (last update 2024-09-16) </span> 
 </h6>
<br> <br>

<h2> <span style="color:cadetblue"> WebPages </span> </h2>   


> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; my CV &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:  &nbsp;&nbsp;&nbsp; <a href="https://www.overleaf.com/read/nwkfcxxpwktg#853360"> Overleaf</a>


> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; InpireHEP &nbsp;&nbsp;:  &nbsp;&nbsp;&nbsp; <a href="https://inspirehep.net/authors/1467863"> InpireHEP:1467863</a>

> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;ORCID &nbsp; : &nbsp; &nbsp;  <a href="https://orcid.org/0000-0002-1689-442X">
ORCID:0000-0002-1689-442X</a>

> &nbsp; Google Scholar&nbsp; : &nbsp; &nbsp;  <a href="https://scholar.google.co.uk/citations?user=XgugRF0AAAAJ&hl=en">
Scholar:XgugRF0AAAAJ</a>

> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GitLab &nbsp; :  &nbsp; &nbsp; <a href="https://gitlab.com/pedrosimpas">
GitLab:pedrosimpas</a>


<h2> <span style="color:cadetblue"> Computer Skills </span> </h2>  

> Computer Languages : 
>> Python, C++, C, and Wolfram Mathematica 

> Computer Tools : 
>> GLoBES, GENIE, GiBUU, Classy, LoopTools, MadGraph, GSL, Latex, and Markdown

<h2> <span style="color:cadetblue"> Languages </span> </h2> 

|          | Skill  | Understanding  | Speaking   | Writting  |    
|--------------|---------------|---------------|--------------------|--------------|
| Portuguese  | Native    | very good | very good    |   very good |
| English  | Fluent    | very good | very good    |   very good |
| Spanish  | Fluent    | very good | good    |   basic |


<h2> <span style="color:cadetblue"> Experience </span> </h2> 

<h4> <span style="color:darkcyan"> Academic </span> </h4> 

<table>
  <tr>
    <td>Colaborator Researcher, Universidade Estadual de Campinas, Campinas, Brazil.</td>
    <td>    </td>
    <td>Ago/2024 to Present</td>
  </tr>
  <tr>
    <td>Postdoc Researcher, The University of Tokyo, Tokyo, Japan.</td>
    <td>    </td>
    <td>Sep/2023 to Mar/2024</td>
  </tr>
  <tr>
    <td>Postdoc Researcher, Tsung-Dao Lee Institute, Shanghai Jiao Tong University, Shanghai, China.</td>
    <td>    </td>
    <td>Jan/2020 to Aug/2023</td>
  </tr>
  <tr>
    <td>Postdoc Researcher, São Paulo State University, Brazil.</td>
    <td>    </td>
    <td>May/2019 to Nov/2019</td>
  </tr>
  <tr>
    <td>Ph.D. in Physics, University of Campinas-Unicamp, Brazil.</td>
    <td>    </td>
    <td>2013 to 2019</td>
  </tr>
  <tr>
    <td>Master in Physics, University of Campinas-Unicamp, Brazil.</td>
    <td>    </td>
    <td>2011 to 2013</td>
  </tr>
  <tr>
    <td>Bachelor in Physics, University of Campinas-Unicamp, Brazil.</td>
    <td>    </td>
    <td>2007 to 2011</td>
  </tr>
</table>

<h4> <span style="color:darkcyan"> Visiting Researcher </span> </h4> 

<table>
  <tr>
    <td>Ph.D. Research Exchange, Fermilab, USA through NPC fellowship</td>
    <td>    </td>
    <td>May/2018 to Oct/2018</td>
  </tr>
  <tr>
    <td>Ph.D. Research Exchange, Northwestern University, USA</td>
    <td>    </td>
    <td>Dec/2017 to Mar/2018</td>
  </tr>
  <tr>
    <td>Ph.D. Research Exchange, University of Valencia, Spain</td>
    <td>    </td>
    <td>Oct/2015 to Oct/2016</td>
  </tr>
  <tr>
    <td>Research Internship, Brazilian Synchroton Light Laboratory, Brazil</td>
    <td>    </td>
    <td>2008 to 2010</td>
  </tr>
</table>


<h4> <span style="color:darkcyan"> Teaching </span> </h4> 

<table>
  <tr>
    <td>Teaching assistant, Physics II, University of Campinas, Brazil. Textbook: Halliday Vol. II</td>
    <td>    </td>
    <td>1st Semester 2015</td>
  </tr>
  <tr>
    <td>Teaching assistant, Physics I, University of Campinas, Brazil. Textbook: Halliday Vol. I</td>
    <td>    </td>
    <td>1st Semester 2014</td>
  </tr>
  <tr>
    <td>Teaching assistant, Topics of physics, University of Campinas, Brazil.</td>
    <td>    </td>
    <td>1st Semester 2013</td>
  </tr>
  <tr>
    <td>Teaching assistant, Topics of physics, University of Campinas, Brazil.</td>
    <td>    </td>
    <td>1st Semester 2012</td>
  </tr>
  <tr>
    <td>Teaching assistant, Physics I, University of Campinas, Brazil. Textbook: Halliday Vol. I</td>
    <td>    </td>
    <td>1st Semester 2012</td>
  </tr>
</table>



<h2> <span style="color:cadetblue"> Awards </span> </h2>  

> 2020 IFGW thesis award:, Best PhD thesis finished at Year 2019, in The Institute
of Physics Gleb Wataghin

> 2020 CAPES thesis award:, Best Brazilian PhD thesis finished at Year 2019 , in
the area of Astronomy/Physics.


<h2> <span style="color:cadetblue"> List of Publications </span> </h2>  

[1] S.-F. Ge, P. Pasquini, and L.~Tan,  
''<em>Neutrino mass measurement with cosmic gravitational focusing</em>'',    
<a href="http://dx.doi.org/10.1088/1475-7516/2024/05/108">JCAP 05, 108 (2024)</a>    
[<a href="http://arxiv.org/abs/2312.16972"> arXiv:2312.16972</a> [hep-ph]]    .

[2] S.-F. Ge, C.F. Kong, and P. Pasquini,    
''<em>Neutrino CP measurement in the presence of RG running with mismatched momentum transfers</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.110.015003">Phys. Rev. D 110, no.1, 1 (2024)</a>    
[<a href="http://arxiv.org/abs/2310.04077"> arXiv:2310.04077</a> [hep-ph]]    .

[3] S.-F. Ge and P. Pasquini,    
''<em>Disentangle neutrino electromagnetic properties with atomic radiative pair emission</em>'',    
<a href="http://dx.doi.org/10.1007/JHEP12(2023)083">JHEP 12, 083 (2023)</a>    
[<a href="http://arxiv.org/abs/2306.12953"> arXiv:2306.12953</a> [hep-ph]]    .

[4] S.-F. Ge and P. Pasquini,    
''<em>Unique probe of neutrino electromagnetic moments with radiative pair emission</em>'',    
<a href="http://dx.doi.org/10.1016/j.physletb.2023.137911">Phys. Lett. B 841 (2023), 137911</a>    
[<a href="http://arxiv.org/abs/2206.11717"> arXiv:2206.11717</a> [hep-ph]]    .

[5] S.-F. Ge, C. F. Kong and P. Pasquini,    
''<em>Improving CP measurement with THEIA and muon decay at rest</em>'',    
<a href="http://dx.doi.org/10.1140/epjc/s10052-022-10478-8">Eur. Phys. J. C 82 (2022) no.6, 572</a>    
[<a href="http://arxiv.org/abs/2202.05038"> arXiv:2202.05038</a> [hep-ph]]    .

[6] S.-F. Ge, P. Pasquini and J. Sheng,    
''<em>Solar active-sterile neutrino conversion with atomic effects at dark matter direct detection experiments</em>'',    
<a href="http://dx.doi.org/10.1007/JHEP05(2022)088">JHEP 05 (2022), 088</a>    
[<a href="http://arxiv.org/abs/2112.05560"> arXiv:2112.05560</a> [hep-ph]]    .

[7] H. X. Lin, J. Tang, S. Vihonen and P. Pasquini,    
''<em>Nonminimal Lorentz invariance violation in light of the muon anomalous magnetic moment and long-baseline neutrino oscillation data</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.105.096029">Phys. Rev. D 105 (2022) no.9, 096029</a>    
[<a href="http://arxiv.org/abs/2111.14336"> arXiv:2111.14336</a> [hep-ph]]    .

[8] S.-F. Ge and P. Pasquini,    
''<em>Probing light mediators in the radiative emission of neutrino pair</em>'',    
<a href="http://dx.doi.org/10.1140/epjc/s10052-022-10131-4">Eur. Phys. J. C 82 (2022) no.3, 208</a>    
[<a href="http://arxiv.org/abs/2110.03510"> arXiv:2110.03510</a> [hep-ph]]    .

[9] S.-F. Ge, X. D. Ma and P. Pasquini,    
''<em>Probing the dark axion portal with muon anomalous magnetic moment</em>'',    
<a href="http://dx.doi.org/10.1140/epjc/s10052-021-09571-1">Eur. Phys. J. C 81 (2021) no.9, 787</a>    
[<a href="http://arxiv.org/abs/2104.03276"> arXiv:2104.03276</a> [hep-ph]]    .

[10] S.-F. Ge, G. Li, P. Pasquini and M. J. Ramsey-Musolf,    
''<em>CP-violating Higgs Di-tau Decays: Baryogenesis and Higgs Factories</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.103.095027">Phys. Rev. D 103 (2021) no.9, 095027</a>    
[<a href="http://arxiv.org/abs/2012.13922"> arXiv:2012.13922</a> [hep-ph]]    .

[11] S.-F. Ge and P. Pasquini,    
''<em>Parity violation and chiral oscillation of cosmological relic neutrinos</em>'',    
<a href="http://dx.doi.org/10.1016/j.physletb.2020.135961">Phys. Lett. B 811 (2020), 135961</a>    
[<a href="http://arxiv.org/abs/2009.01684"> arXiv:2009.01684</a> [hep-ph]]    .

[12] S.-F. Ge, P. Pasquini and J. Sheng,    
''<em>Solar neutrino scattering with electron into massive sterile neutrino</em>'',    
<a href="http://dx.doi.org/10.1016/j.physletb.2020.135787">Phys. Lett. B 810 (2020), 135787</a>    
[<a href="http://arxiv.org/abs/2006.16069"> arXiv:2006.16069</a> [hep-ph]]    .

[13] L. S. Miranda, P. Pasquini, U. Rahaman and S. Razzaque,    
''<em>Searching for non-unitary neutrino oscillations in the present T2K and NO$\nu $A data</em>'',    
<a href="http://dx.doi.org/10.1140/epjc/s10052-021-09227-0">Eur. Phys. J. C 81 (2021) no.5, 444</a>    
[<a href="http://arxiv.org/abs/1911.09398"> arXiv:1911.09398</a> [hep-ph]]    .

[14] A. De Gouv\^ea, K. J. Kelly, G. V. Stenico and P. Pasquini,    
''<em>Physics with Beam Tau-Neutrino Appearance at DUNE</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.100.016004">Phys. Rev. D 100 (2019) no.1, 016004</a>    
[<a href="http://arxiv.org/abs/1904.07265"> arXiv:1904.07265</a> [hep-ph]]    .

[15] O. G. Miranda, P. Pasquini, M. T\'ortola and J. W. F. Valle,    
''<em>Exploring the Potential of Short-Baseline Physics at Fermilab</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.97.095026">Phys. Rev. D 97 (2018) no.9, 095026</a>    
[<a href="http://arxiv.org/abs/1802.02133"> arXiv:1802.02133</a> [hep-ph]]    .

[16] P. Pasquini,    
''<em>Long-Baseline Oscillation Experiments as a Tool to Probe High Energy Flavor Symmetry Models</em>'',    
<a href="http://dx.doi.org/10.1155/2018/1825874">Adv. High Energy Phys. 2018 (2018), 1825874</a>    
[<a href="http://arxiv.org/abs/1802.00821"> arXiv:1802.00821</a> [hep-ph]]    .

[17] P. Pasquini,    
''<em>Reactor and atmospheric neutrino mixing angles\textquoteright{} correlation as a probe for new physics</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.96.095021">Phys. Rev. D 96 (2017) no.9, 095021</a>    
[<a href="http://arxiv.org/abs/1708.04294"> arXiv:1708.04294</a> [hep-ph]]    .

[18] S. S. Chatterjee, M. Masud, P. Pasquini and J. W. F. Valle,    
''<em>Cornering the revamped BMV model with neutrino oscillation data</em>'',    
<a href="http://dx.doi.org/10.1016/j.physletb.2017.09.052">Phys. Lett. B 774 (2017), 179-182</a>    
[<a href="http://arxiv.org/abs/1708.03290"> arXiv:1708.03290</a> [hep-ph]]    .

[19] S. S. Chatterjee, P. Pasquini and J. W. F. Valle,    
''<em>Resolving the atmospheric octant by an improved measurement of the reactor angle</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.96.011303">Phys. Rev. D 96 (2017) no.1, 011303</a>    
[<a href="http://arxiv.org/abs/1703.03435"> arXiv:1703.03435</a> [hep-ph]]    .

[20] S. S. Chatterjee, P. Pasquini and J. W. F. Valle,    
''<em>Probing atmospheric mixing and leptonic CP violation in current and future long baseline oscillation experiments</em>'',    
<a href="http://dx.doi.org/10.1016/j.physletb.2017.05.080">Phys. Lett. B 771 (2017), 524-531</a>    
[<a href="http://arxiv.org/abs/1702.03160"> arXiv:1702.03160</a> [hep-ph]]    .

[21] P. Pasquini, S. C. Chuli\'a and J. W. F. Valle,    
''<em>Neutrino oscillations from warped flavor symmetry: predictions for long baseline experiments T2K, NOvA and DUNE</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.95.095030">Phys. Rev. D 95 (2017) no.9, 095030</a>    
[<a href="http://arxiv.org/abs/1610.05962"> arXiv:1610.05962</a> [hep-ph]]    .

[22] S.-F. Ge, P. Pasquini, M. Tortola and J. W. F. Valle,    
''<em>Measuring the leptonic CP phase in neutrino oscillations with nonunitary mixing</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.95.033005">Phys. Rev. D 95 (2017) no.3, 033005</a>    
[<a href="http://arxiv.org/abs/1605.01670"> arXiv:1605.01670</a> [hep-ph]]    .

[23] P. S. Pasquini and O. L. G. Peres,    
''<em>Bounds on Neutrino-Scalar Yukawa Coupling</em>'',    
<a href="http://dx.doi.org/10.1103/PhysRevD.93.053007">Phys. Rev. D 93 (2016) no.5, 053007</a>    
[erratum: Phys. Rev. D 93 (2016) no.7, 079902]   
[<a href="http://arxiv.org/abs/1511.01811"> arXiv:1511.01811</a> [hep-ph]].


<h2> <span style="color:cadetblue"> Proceedings </span> </h2>  

[1] S. F. Ge and P. Pasquini,    
''<em>Probing Light Mediators and Neutrino Electromagnetic Moments with Atomic Radiative Emission of Neutrino Pairs</em>'',    
<a href="http://dx.doi.org/10.3390/psf2023008051">Phys. Sci. Forum 8 (2023) no.1, 51</a>.

[2] P. Pasquini, O. Miranda, M. Tortola and J. Valle,    
''<em>Exploring the Potential of Short-Baseline Physics at Fermilab</em>'',    
<a href="http://dx.doi.org/10.22323/1.341.0155">PoS NuFACT2018 (2019), 155</a>.

[3] S. S. Chatterjee, M. Masud, P. Pasquini and J. W. F. Valle,    
''<em>Probing the revamped A4 symmetry at long-baseline neutrino experiments</em>'',    
<a href="http://dx.doi.org/10.22323/1.295.0127">PoS NuFact2017 (2017), 127</a>.

[4] P. Pasquini, S. F. Ge, M. A. T\'ortola and J. W. F. Valle,    
''<em>Measuring the Leptonic CP Phase in Neutrino Oscillations with Non-Unitary Mixing</em>'',    
<a href="http://dx.doi.org/10.22323/1.283.0026">PoS NOW2016 (2017), 026</a>.
